FROM alpine:3.17

MAINTAINER OSiUX "osiux@osiux.com"

RUN apk update \
  && apk add --no-cache ca-certificates emacs bash gawk file imagemagick gifsicle curl make pandoc py3-pip coreutils git graphviz ttf-inconsolata \
  && pip3 install --no-cache-dir md2gemini

WORKDIR /root

COPY init.el /root/.emacs.d/
COPY entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "emacs" ]
