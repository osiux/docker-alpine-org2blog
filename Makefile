build_latest:
	docker build -t alpine-org2blog --no-cache .
	docker image tag alpine-org2blog os1ux/alpine-org2blog:latest
	docker image ls | grep alpine-org2blog
	docker image push os1ux/alpine-org2blog:latest
