# `os1ux/alpine-org2blog`

_Docker_ image for _Emacs_ on _Alpine Linux_ to export to _Markdown_ and
_Gemini_ using `pandoc`.

[![Docker Pulls](https://img.shields.io/docker/pulls/os1ux/alpine-org2blog)](https://hub.docker.com/r/os1ux/alpine-org2blog/)
[![Docker Image Size (latest by date)](https://img.shields.io/docker/image-size/os1ux/alpine-org2blog)](https://hub.docker.com/r/os1ux/alpine-org2blog/)

Based on https://github.com/iquiw/docker-alpine-emacs

_Emacs_ from _Alpine_ community repository with _MELPA_ setting.

| _package_   | _version_ |
|-------------|-----------|
| `emacs`     | `v28.2`   |
| `pandoc`    | `2.19.2`  |
| `md2gemini` | `1.9.0`   |

## How to use this image

```bash

docker run -it os1ux/alpine-org2blog
```

## Environment Variables

### `EMACS_PACKAGES`

This variable specifies space separated list of packages.
They are installed automatically when the container is executed.

## License

_GNU General Public License, GPLv3._

## Author Information

This repo was created in 2023 by
 [Osiris Alejandro Gomez](https://osiux.com/), worker cooperative of
 [gcoop Cooperativa de Software Libre](https://www.gcoop.coop/).
